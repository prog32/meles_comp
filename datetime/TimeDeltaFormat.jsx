import React from 'react';

import SmartTimeLeft from './time_format/SmartTimeLeft';
import CompactTime from './time_format/CompactTime';
import { secondsToDHMS } from './utils';

const SECONDS_IN_DAY = (3600 * 24);
const SECONDS_IN_HOUR = 3600;


export const TimeDeltaFormat = ({ totalSeconds, format, children, compact }) => {

  const [days, hours, minutes, seconds] = secondsToDHMS(totalSeconds);

  if (format === "smart"){

    return <span>
      za{' '}
      <SmartTimeLeft
        days={days}
        hours={hours}
        minutes={minutes}
        seconds={seconds}
      />
    </span>
  }

  if (compact){
    return <CompactTime
      format={format}
      days={days}
      hours={hours}
      minutes={minutes}
      seconds={seconds}
    />
  }
  if (format === "days"){
    return <span>{days} {days === 1 ? "dzień" : "dni"}</span>;
  } if (format === "days_or_hms"){
    if (days === 0){
      return (
        <span>{hours} {hours === 1 ? "godzina": "godzin" } {minutes } minut {seconds} sekund</span>
      );
    }
    return <span>{days} {days === 1 ? "dzień" : "dni"}</span>;

  }
  return (
    <span>{days} {days === 1 ? "dzień" : "dni"} {hours} {hours === 1 ? "godzina": "godzin" } {minutes } minut {seconds} sekund</span>
  );

};



export default TimeDeltaFormat;
