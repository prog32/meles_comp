import React from 'react';

import { useState, useEffect } from "react";

import TimeDeltaFormat from './TimeDeltaFormat';

const SECONDS_IN_DAY = (3600 * 24);
const SECONDS_IN_HOUR = 3600;


export const TimeDeltaDisplay = ({ totalSeconds, format, children, compact }) => {

  const [sec, setSec] = useState(0);
  // --------------------
  let refreshSeconds = 1;
  if (totalSeconds > SECONDS_IN_DAY){
    // Days scale: refresh every hour
    refreshSeconds = SECONDS_IN_HOUR;
  } else if (totalSeconds > SECONDS_IN_HOUR){
    // Hours scale: refresh every minute
    refreshSeconds = 60;
  } else {
    // refresh every second
    refreshSeconds = 1;
  }

  useEffect(() => {
    const interval = setInterval(() => {
      setSec(sec => sec + 1);
    },
    1000 * refreshSeconds
    );
    return () => clearInterval(interval);
  }, []);
  // --------------------


  return <TimeDeltaFormat
    totalSeconds={totalSeconds}
    format={format}
    compact={compact}
  >{children}</TimeDeltaFormat>

};



export default TimeDeltaDisplay;
