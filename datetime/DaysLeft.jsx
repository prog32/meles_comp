import React from 'react';

import parseISO from 'date-fns/parseISO';
import differenceInSeconds from 'date-fns/differenceInSeconds';
import TimeDeltaDisplay from './TimeDeltaDisplay';


export const DaysLeft = ({date}) => {
  date = parseISO(date);
  let totalSeconds = differenceInSeconds(date, new Date());


  return <TimeDeltaDisplay
    totalSeconds={totalSeconds}
    format={"days"}
  ></TimeDeltaDisplay>

}



export default DaysLeft;
