import React from "react";

import getTimeLeft from './getTimeLeft';
import TimeDeltaDisplay from "./TimeDeltaDisplay";


export const TimeSince = ({date, format, children, since, compact}) => {
  let dateObj = date;
  if (dateObj === null || dateObj === undefined){
    dateObj = children;
  }

  let totalSeconds = 0 - getTimeLeft(dateObj);

  if (totalSeconds <= 0){
    // since = true;
    return null;
    // totalSeconds = 0;
  }


  return <TimeDeltaDisplay
    totalSeconds={totalSeconds}
    since={true}
    format={format}
    compact={compact}
  >{children}</TimeDeltaDisplay>;

}

export default TimeSince;
