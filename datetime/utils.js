import parseISO from "date-fns/parseISO";
import differenceInSeconds from "date-fns/differenceInSeconds";

const SECONDS_IN_DAY = (3600 * 24);
const SECONDS_IN_HOUR = 3600;

export const secondsToDHMS = totalSeconds => {
  let val = totalSeconds;
  const days = Math.floor(val / SECONDS_IN_DAY);
  val %= SECONDS_IN_DAY;
  const hours = Math.floor(val / SECONDS_IN_HOUR);
  val %= SECONDS_IN_HOUR;
  const minutes = Math.floor(val / 60);
  const seconds = val % 60;
  return [days, hours, minutes, seconds];
};





export const getTimeLeft = (date) => {

  let dateObj = date;

  if (dateObj === null || dateObj === undefined){
    return null;
  }

  if (typeof dateObj === "string" || dateObj instanceof String){
    dateObj = parseISO(dateObj);
  } else if (typeof value === 'number') {
      dateObj = new Date(dateObj);
  }


  const now = new Date();
  return differenceInSeconds(dateObj, now);
};

// TimeLeft.default
// since
