import React from 'react';

import parseISO from "date-fns/parseISO";
import differenceInSeconds from "date-fns/differenceInSeconds";

import TimeDeltaDisplay from "./TimeDeltaDisplay";


export const TimeLeft = ({ date, format, children, compact }) => {


  let dateObj = date;
  if (dateObj === null || dateObj === undefined){
    dateObj = children;
  }

  if (dateObj === null || dateObj === undefined){
    return null;
  }

  if (typeof dateObj === "string" || dateObj instanceof String){
    dateObj = parseISO(dateObj);
  }

  const now = new Date();
  let since = false;
  let totalSeconds = differenceInSeconds(dateObj, now);

  if (totalSeconds < 0){
    since = true;
    totalSeconds = 0 - totalSeconds;
  }


  return <TimeDeltaDisplay
    totalSeconds={totalSeconds}
    since={since}
    format={format}
    compact={compact}
  >{children}</TimeDeltaDisplay>;

};



export default TimeLeft;
