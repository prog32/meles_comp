import React from 'react';

import parseISO from 'date-fns/parseISO';

import TimeDeltaDisplay from './TimeDeltaDisplay';
import useCountDown from 'mh/useCountDown';


export const TimeLeftCountDown = ({date, children, compact, format}) => {

  const secondsLeft = useCountDown(parseISO(date));
  if (secondsLeft <= 0){
    return null;
  }


  return <TimeDeltaDisplay
    totalSeconds={secondsLeft}
    format={format}
    compact={compact}
  >{children}</TimeDeltaDisplay>

}



export default TimeLeftCountDown;
