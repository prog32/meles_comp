import React from 'react';

import {default as dateFormatter} from 'date-fns/format';
import parseISO from 'date-fns/parseISO';


export const FormatDate = ({date, format, children}) => {
  if (date === null || date === undefined){
    date = children;
  }

  if (date === null || date === undefined){
      return null;
  }

  if (format === null || format === undefined){
    format = "yyyy-MM-dd HH:mm:ss";
  }

  let dateObj = date;
  if (typeof dateObj === "string" || dateObj instanceof String){
    dateObj = parseISO(dateObj);
  } else if (typeof value === 'number') {
      dateObj = new Date(dateObj);
  }

  date = dateFormatter(dateObj, format);

  return (
    <span>{date}</span>
  );
}


export const FormattedDateM = ({date}) => {
  return <FormatDate date={date} format='yyyy-MM-dd HH:mm' />
}

export const FDate = ({date}) => {
  return <FormatDate date={date} format='yyyy-MM-dd' />
}
export const FTime = ({date}) => {
  return <FormatDate date={date} format='yyyy-MM-dd HH:mm' />
}


export default FormatDate;
