import React from 'react';

import { getTimeLeft } from './utils';
import TimeDeltaDisplay from "./TimeDeltaDisplay";


export const TimeLeft = ({ date, format, children, compact }) => {

  let dateObj = date;
  if (dateObj === null || dateObj === undefined){
    dateObj = children;
  }

  let totalSeconds = getTimeLeft(dateObj);

  if (totalSeconds <= 0){
    // since = true;
    return null;
    // totalSeconds = 0;
  }


  return <TimeDeltaDisplay
    totalSeconds={totalSeconds}
    since={false}
    format={format}
    compact={compact}
  >{children}</TimeDeltaDisplay>;

};

// TimeLeft.default
// since


export default TimeLeft;
