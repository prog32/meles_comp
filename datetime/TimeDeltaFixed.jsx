import React from 'react';

import parseISO from "date-fns/parseISO";
import differenceInSeconds from "date-fns/differenceInSeconds";

import TimeDeltaDisplay from "./TimeDeltaDisplay";


export const TimeDeltaFixed = ({ date, date2, format, children, compact }) => {


  let dateObj = date;
  let dateObj2 = date2;

  if (dateObj === null || dateObj === undefined){
    dateObj = children;
  }

  if (dateObj === null || dateObj === undefined){
    return null;
  }

  if (typeof dateObj === "string" || dateObj instanceof String){
    dateObj = parseISO(dateObj);
  }
  if (dateObj2 === null || dateObj2 === undefined){
    dateObj2 = new Date();
  }

  if (typeof dateObj2 === "string" || dateObj2 instanceof String){
    dateObj2 = parseISO(dateObj2);
  }


  let since = false;
  let totalSeconds = differenceInSeconds(dateObj, dateObj2);

  if (totalSeconds < 0){
    since = true;
    totalSeconds = 0 - totalSeconds;
  }


  return <TimeDeltaDisplay
    totalSeconds={totalSeconds}
    since={since}
    format={format}
    compact={compact}
  >{children}</TimeDeltaDisplay>;

};



export default TimeDeltaFixed;
