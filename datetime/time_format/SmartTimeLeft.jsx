import React from "react";

const daysName = (val) => {
    // 0   dni  # ukryć
    // 1   dzień
    // 2-* dni

    if (val === 1){
        return "dzień"
    }
    return "dni";
}

const hoursName = (val) => {
    // 0 godzin ukryć
    // 1 godzinę
    // 2-23 godziny
    if (val === 0){
        return "godzin"
    }
    if (val === 1){
        return "godzinę"
    }
    return "godziny";
}
const minutesName = (val) => {
    // 0 minut  # ukryć
    // 1 minutę
    // 2-4 minuty
    // 5+ minut

    if (val === 1){
        return "minutę"
    }
    if (val >= 2 && val <= 4){
        return "minuty";
    }
    return "minut";
}
const secondsName = (val) => {
    // 0 sekund   #ukryć
    // 1 sekundę
    // 2-4 sekundy
    // 5-59 sekund

    if (val === 1){
        return "sekundę"
    }
    if (val >= 2 && val <= 4){
        return "sekundy";
    }
    return "sekund";
}

export const SmartTimeLeft = ({days, hours, minutes, seconds}) => {

    if (days === 0){

        if (hours === 0){
            // days = 0
            // hours = 0


            if (minutes === 0){
                // days = 0
                // hours = 0
                // minutes = 0
                return <span>
                    {seconds} {secondsName(seconds)}
                </span>
            }

            // days = 0
            // hours = 0
            // minutes > 0
            return <span>
                {minutes} {minutesName(minutes)} i {seconds} {secondsName(seconds)}
            </span>
        }


        // days = 0
        // hours > 0
        return <span>
            {hours} {hoursName(hours)} i {minutes} {minutesName(minutes)}
        </span>
    }


    // Show only: days & hours
    // days > 0
    return <span>
        {days} {daysName(days)} i {hours} {hoursName(hours)}
    </span>


}

export default SmartTimeLeft;
