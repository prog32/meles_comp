import React from "react";

const pad = val => {
  if (val < 10){
    return `0${  val}`;
  }
  return val;
};


export const CompactTime = ({format, days, hours, minutes, seconds}) => {

    if (format === "days"){
      return <span>{days} {days === 1 ? "dzień" : "dni"}</span>;
    } if (format === "days_or_hms"){
      if (days === 0){
        if (hours === 0){
          return (
            <span>{hours}:{pad(minutes)}:{pad(seconds)}</span>
          );
        }
        return (
          <span>{hours}:{pad(minutes)}</span>
        );

      }
      return <span>{days} {days === 1 ? "dzień" : "dni"}</span>;

    }
    return (
      <span>{days} {days === 1 ? "dzień" : "dni"} {hours}:{pad(minutes)}:{pad(seconds)}</span>
    );


}

export default CompactTime;
