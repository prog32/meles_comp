import React from 'react';

import parseISO from 'date-fns/parseISO';
import differenceInSeconds from 'date-fns/differenceInSeconds';
import TimeDeltaDisplay from './TimeDeltaDisplay';


export const DaysSince = ({date}) => {
  date = parseISO(date);
  let totalSeconds = differenceInSeconds(new Date(), date);


  return <TimeDeltaDisplay
    totalSeconds={totalSeconds}
    format={"days"}
  ></TimeDeltaDisplay>

}



export default DaysSince;
